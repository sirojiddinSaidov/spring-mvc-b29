package uz.pdp.config;


import jakarta.servlet.MultipartConfigElement;
import jakarta.servlet.ServletRegistration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MyApplicationConfig extends AbstractAnnotationConfigDispatcherServletInitializer {


    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{MyWebMVCConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        // MultipartConfig annotation = PartConfig.class.getAnnotation(MultipartConfig.class);

        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(
                "",
                3_000_000,
                30_000_000,
                1_000_000
        );

        registration.setMultipartConfig(multipartConfigElement);
    }
}
