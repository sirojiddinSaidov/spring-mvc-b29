package uz.pdp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:application.properties")
public class DataSourceConfig {

    @Value(value = "${app.jdbc.url}")
    private String url;

    @Value(value = "${app.jdbc.username}")
    private String username;

    @Value(value = "${app.jdbc.password}")
    private String password;

    @Bean
    public NamedParameterJdbcTemplate jdbcTemplate() throws ClassNotFoundException {
        DataSource dataSource = new DriverManagerDataSource(
                url,
                username,
                password);
        Class.forName("org.postgresql.Driver");
        return new NamedParameterJdbcTemplate(dataSource);
    }
}
