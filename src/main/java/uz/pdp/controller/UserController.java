package uz.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import uz.pdp.dao.UserDAO;
import uz.pdp.model.User;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    private final UserDAO userDAO;

    public UserController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }


    @GetMapping
    public ModelAndView users(@RequestParam(defaultValue = "1") int page,
                              @RequestParam(defaultValue = "10") int size,
                              @RequestParam(defaultValue = "") String search,
                              @RequestParam(defaultValue = "id") String sort
    ) {
        ModelAndView modelAndView = new ModelAndView();
        long count = userDAO.getCountUser(search);
        long pageCount = count / size;
        pageCount = (count % size) > 0 ? pageCount + 1 : pageCount;
        modelAndView.setViewName("user/index");
        modelAndView.addObject("users", userDAO.users(page, size, search, sort));
        modelAndView.addObject("pageCount", pageCount);
        modelAndView.addObject("currentPage", page);
        modelAndView.addObject("size", size);
        modelAndView.addObject("leftMin", Math.max((page - 2), 0));
        modelAndView.addObject("leftMax", Math.min(page + 2, pageCount));
        modelAndView.addObject("rightMin", Math.min(pageCount - 5, pageCount));
//        modelAndView.addObject("rightMin", Math.min(pageCount - 5, pageCount));

        //(15 % 10) + 1
        //(10 / 10) + 1

        return modelAndView;
    }

    @GetMapping("/{id}")
    public String getEditForm(@PathVariable Integer id,
                              Model model) {
        User one = userDAO.one(id);
        model.addAttribute("user", one);
        return "user/edit";
    }

    @PostMapping("/{id}")
    public String editUser(@PathVariable Integer id,
                           @ModelAttribute User user) {
        userDAO.update(id, user);
        return "redirect:/user";
    }

    @PostMapping("/remove")
    public String deleteUser(
            @ModelAttribute User user) {
        userDAO.delete(user.getId());
        return "redirect:/user";
    }

    @PostMapping("/edit")
    public String edit(@ModelAttribute User user) {
        userDAO.update(user.getId(), user);
        return "redirect:/user";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute User user) {
        userDAO.save(user);
        return "redirect:/user";
    }

}