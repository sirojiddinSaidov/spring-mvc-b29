package uz.pdp.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import uz.pdp.dao.UserDAO;
import uz.pdp.model.User;

import java.util.List;

@Controller
public class HomeController {

    private final MessageSource messageSource;

    private final UserDAO userDAO;

    public HomeController(
            MessageSource messageSource, UserDAO userDAO) {
        this.messageSource = messageSource;
        this.userDAO = userDAO;
    }

    @GetMapping(value = {"/", "/home"})
    public String testPage() {
        System.out.println();
        return "index";
    }

    @GetMapping("/test")
    @ResponseBody
    public String test(HttpServletRequest request) {
        if (true)
            return messageSource.getMessage("successfullyAdded",
                    null,
                    LocaleContextHolder.getLocale());
        else
            return messageSource.getMessage("errorInAdded", null, LocaleContextHolder.getLocale());
    }


    @GetMapping(value = "/my-user")
    @ResponseBody
    public List<User> list(HttpServletResponse response) {
//        response.setHeader("Content-Type", "application/json");
        return userDAO.users();
    }

}