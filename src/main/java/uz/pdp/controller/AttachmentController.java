package uz.pdp.controller;


import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.dao.AttachmentDAO;
import uz.pdp.model.Attachment;
import uz.pdp.model.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/attachment")
@RequiredArgsConstructor
public class AttachmentController {

    static final String FILE_BASE_PATH = "D:\\ketmon\\";

    private final AttachmentDAO attachmentDAO;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("files", List.of());
        return "attachment/index";
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("ketmon") MultipartFile multipartFile) throws IOException {
        String folderPath = getFolderPath();
        String fullPath = getFullPath(multipartFile.getOriginalFilename(), folderPath);

        saveAttachment(multipartFile, fullPath);

        new File(folderPath).mkdirs();

        Files.copy(multipartFile.getInputStream(),
                Path.of(fullPath),
                StandardCopyOption.REPLACE_EXISTING);

        return "redirect:/attachment";
    }


    @PostMapping("/upload2")
    public String uploadFile2(@RequestParam("ketmon") MultipartFile[] multipartFiles) throws IOException {
        for (MultipartFile multipartFile : multipartFiles) {
            UUID uuid = UUID.randomUUID();
            String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());

            Files.copy(multipartFile.getInputStream(),
                    Path.of("D:\\ketmon\\" + uuid + "." + extension),
                    StandardCopyOption.REPLACE_EXISTING);
        }

        return "redirect:/attachment";
    }

    @GetMapping("download")//   http://localhost:8080/attachment/download?name=JOIN.png
    public void download(@RequestParam String name, HttpServletResponse response) throws IOException {
        File file = new File("D:\\ketmon\\" + name);
//        FileInputStream fileInputStream = new FileInputStream(file);
//        response.getOutputStream().write(fileInputStream.readAllBytes());
        Files.copy(Path.of("D:\\ketmon\\" + name), response.getOutputStream());
        response.setContentType("image/png");
        response.setHeader("Content-Disposition", "attachment; filename=" + name);
        response.setContentLength((int) file.length());
    }


    @GetMapping("/download/{name}/{type}")
    public ResponseEntity<?> download2(@PathVariable String name,
                                       @PathVariable String type) throws IOException {
        File file = new File("D:\\ketmon\\" + name);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_PNG)
                .headers(httpHeaders -> httpHeaders.set(
                        "Content-Disposition", type + "; filename=" + name
                ))
                .body(new FileInputStream(file).readAllBytes());
    }

    @GetMapping("/uxla")
    public String getSecondPage() {
        return "attachment/second";
    }

    @PostMapping("upload-with-bla")
    public String uploadBla(@ModelAttribute User user,
                            @RequestParam("ketmon") MultipartFile[] multipartFiles) {
        System.out.println(user);
        return "redirect:/attachment/uxla";
    }

    @PostMapping("upload-with-bla2")
    public String uploadBla(@ModelAttribute User user) {
        System.out.println(user);
        return "redirect:/attachment/uxla";
    }


    @GetMapping("/last-download/{id}")
    public ResponseEntity<?> downloadFile(@PathVariable Integer id) throws IOException {
        Attachment attachment = attachmentDAO.one(id);

        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(attachment.getContentType()))
                .headers(httpHeaders -> httpHeaders.set(
                        "Content-Disposition", "attachment; filename=" + attachment.getName()
                ))
                .body(new FileInputStream(attachment.getUrl()).readAllBytes());
    }

    private String getFolderPath() {
        LocalDate now = LocalDate.now();
        int year = now.getYear();
        int month = now.getMonthValue();
        int day = now.getDayOfMonth();
        return FILE_BASE_PATH + year + "\\" + month + "\\" + day;
    }

    private String getFullPath(String originalName, String folderPath) {
        UUID uuid = UUID.randomUUID();
        String extension = FilenameUtils.getExtension(originalName);
        return folderPath + "\\" + uuid + "." + extension;
    }

    private void saveAttachment(MultipartFile multipartFile, String url) {
        attachmentDAO.save(Attachment.builder()
                .name(multipartFile.getOriginalFilename())
                .url(url)
                .size(multipartFile.getSize())
                .contentType(multipartFile.getContentType())
                .build());
    }

}
