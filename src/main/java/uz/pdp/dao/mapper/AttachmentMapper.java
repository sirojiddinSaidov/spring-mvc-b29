package uz.pdp.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import uz.pdp.model.Attachment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class AttachmentMapper implements RowMapper<Attachment> {
    @Override
    public Attachment mapRow(ResultSet rs, int rowNum) throws SQLException {
        return Attachment.builder()
                .id(rs.getInt("id"))
                .name(rs.getString("name"))
                .url(rs.getString("url"))
                .contentType(rs.getString("content_type"))
                .size(rs.getLong("size"))
                .build();
    }
}
