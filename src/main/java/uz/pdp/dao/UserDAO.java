package uz.pdp.dao;

import uz.pdp.model.User;

import java.util.List;

public interface UserDAO {

    List<User> users(int page, int size, String search, String sort);

    List<User> users(int page, int size);

    List<User> users();

    User one(Integer id);

    int update(Integer id, User user);

    int save(User user);

    int delete(Integer id);

    long getCountUser(String search);

}
