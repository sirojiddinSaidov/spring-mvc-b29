package uz.pdp.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import uz.pdp.dao.mapper.AttachmentMapper;
import uz.pdp.model.Attachment;
import uz.pdp.model.User;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class AttachmentDAOImpl implements AttachmentDAO {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<Attachment> list() {
        return jdbcTemplate.query("SELECT * FROM attachment",
                new AttachmentMapper());
    }

    @Override
    public Attachment one(Integer id) {
        SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);
        return jdbcTemplate.queryForObject("SELECT * FROM attachment WHERE id = :id",
                parameterSource,
                new AttachmentMapper());
    }

    @Override
    public int save(Attachment attachment) {
        SqlParameterSource parameterSource = new MapSqlParameterSource(
                "name", attachment.getName())
                .addValue("url", attachment.getUrl())
                .addValue("size", attachment.getSize())
                .addValue("contentType", attachment.getContentType());
        return jdbcTemplate.update("INSERT INTO attachment(name, url,content_type,size) VALUES(:name, :url, :contentType, :size)",
                parameterSource
        );
    }

    @Override
    public int delete(Integer id) {
        return 0;
    }
}
