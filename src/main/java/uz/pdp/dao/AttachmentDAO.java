package uz.pdp.dao;

import uz.pdp.model.Attachment;
import uz.pdp.model.User;

import java.util.List;

public interface AttachmentDAO {

    List<Attachment> list();

    Attachment one(Integer id);

    int save(Attachment attachment);

    int delete(Integer id);
}
