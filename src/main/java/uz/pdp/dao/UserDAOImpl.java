package uz.pdp.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import uz.pdp.dao.mapper.UserMapper;
import uz.pdp.model.User;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class UserDAOImpl implements UserDAO {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<User> users(int page, int size, String search, String sort) {

        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("search", search)
                .addValue("size", size)
                .addValue("offset", (page - 1) * size);
        String query = String.format("SELECT * FROM users WHERE first_name ~* :search OR last_name ~* :search ORDER BY %s LIMIT :size OFFSET :offset", sort);
        return jdbcTemplate.query(
                query,
                parameterSource,
                new UserMapper()
        );
    }

    @Override
    public List<User> users(int page, int size) {
        return users(page, size, "", "id");
    }

    @Override
    public List<User> users() {
        return users(1, 10, "", "id");
    }

    @Override
    public User one(Integer id) {
        SqlParameterSource parameterSource =
                new MapSqlParameterSource("ketmon", id);
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE id = :ketmon",
                parameterSource,
                new UserMapper());
    }

    @Override
    public int update(Integer id, User user) {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("fName", user.getFirstName())
                .addValue("lName", user.getLastName())
                .addValue("i_id", id);
        return jdbcTemplate.update(
                "UPDATE users SET first_name = :fName, last_name = :lName WHERE id = :i_id",
                parameterSource);
    }

    @Override
    public int save(User user) {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("fName", user.getFirstName())
                .addValue("lName", user.getLastName());
        return jdbcTemplate.update(
                "INSERT INTO users(first_name, last_name)  VALUES (:fName, :lName)",
                parameterSource);
    }

    @Override
    public int delete(Integer id) {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("i_id", id);
        return jdbcTemplate.update(
                "DELETE FROM users WHERE id = :i_id",
                parameterSource);
    }

    @Override
    public long getCountUser(String search) {
        SqlParameterSource parameterSource = new MapSqlParameterSource("search", search);
        return
                jdbcTemplate.queryForObject("SELECT COUNT(id) FROM users WHERE first_name ~* :search OR last_name ~* :search", parameterSource, Long.class);
    }
}
