package uz.pdp.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Attachment {

    private Integer id;

    private String name;

    private String url;

    private long size;

    private String contentType;
}
